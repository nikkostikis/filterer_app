This is an application created for a Coursera MOOC. 

Course title: iOS App Development Basics

Instructor: Parham Aarabi

Provider: University of Toronto

Link: https://www.coursera.org/learn/ios-app-development-basics

Course summary: 
iOS App Development Basics, the second course in the iOS App Development with Swift specialization, expands your programming skills and applies them to authentic app development projects. The topics covered in this course include Xcode basics, Core iOS and Cocoa Touch frameworks, simple user interface creation, MVC Architecture and much more.  With a focus on using Apple’s components to access sensors like camera, microphone and GPS, by the end of this course you will be able to create a basic App according to specified parameters and guidelines.

Session Date: December 2015 

/////////////////////////////////////////////////////////

The app conforms to all the specifications and then some. 

-The filtered image is responsive to the long press and release.

-The filters are applied through a collection view with my hand-drawn-face icon with the respective hues and filters. I also used my face for the app icon. I like my face. The code for the old button-y secondary menu is commented out. The view exists in the storyboard but never added to the main view. 

-While the app is running it prints helper messages to the console, have a look at it while testing it.

-The filters that should, have an edit option.

-The buttons are enabled only when they matter.

-There are beautiful transitions everywhere. Ok, rather basic transitions actually. And not everywhere.


Changes and improvements not included in the specifications/requirements:

-The tasks are multithreaded. Both the call for the filters so that the app UI remains responsive, and the filters themselves. For the spinner and the dispatch_async I have to send my shout out to Kumar Gaurav who made an excellent post in the forum. The multithreaded implementation of the filters is explained in the comments of the NKImageProc file. I used NSOperationQueue, inspired by a post in StackOverflow. The app can perform decently even in my old and beat up 4s. If the images are small it’s super fast. HyperFast, I would describe it. If you blink, you’ll miss it. 

-The RGBAImage is now a class, to avoid memory leaks. When an object is not in use it is now properly deinitialized. Before, the RGBAImage structs would remain live and leak the memory. Shout out to David Lin for suggesting that change in the forum.

-There was an orientation problem which was dealt with by altering the RGBAImage implementation. Big shout out to Kumar Gaurav and João Pedro de Matos Neves for their invaluable posts in the forum.

-For all you lazy people, there is a video prepared and published (nikthegeek.gr/filterer/filterer.mp4) to show how the app performs. 



See you later!
Nick.
