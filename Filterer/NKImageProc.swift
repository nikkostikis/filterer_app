//The filters are calculated in a multithreaded fashion
//Wherever there is a double for loop, for the x, y dimensions
//constructing the one-dimensional index, the second for loop is
//inside a block added to an NSOperationQueue. 
//This makes the y for loops run independently in different threads.
//In order to assure that the filtering of the image has finished 
//before returning the result to the view, the
//operationQueue.wait... command is issued. 

import UIKit

public class NKImageProc {
    // an RGBA instance as a property
    var rgbaIm: RGBAImage
    
    // the [rgb] average value of the rgbaIm as a computed property
    public var avgColor: [Int] {
        var sumred: Int = 0
        var sumgreen: Int = 0
        var sumblue: Int = 0
        for y in 0..<self.rgbaIm.height {
            for x in 0..<self.rgbaIm.width {
                let index = y * self.rgbaIm.width + x
                var pixel = self.rgbaIm.pixels[index]
                
                sumred = sumred + Int(pixel.red)
                sumgreen = sumgreen + Int(pixel.green)
                sumblue = sumblue + Int(pixel.blue)
            }
        }
        sumred = Int(sumred / (self.rgbaIm.height * self.rgbaIm.width))
        sumgreen = Int(sumgreen / (self.rgbaIm.height * self.rgbaIm.width))
        sumblue = Int(sumblue / (self.rgbaIm.height * self.rgbaIm.width))
        return [sumred, sumgreen, sumblue]
    }
    
    // initializer
    public init(image: RGBAImage) {
        self.rgbaIm = image
    }
    
    // filter to change the brightness per perc percent (+/- supported)
    public func changeBrightness(perc: Double) {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    pixel.red = UInt8( max( min(255, Double(pixel.red) + Double(pixel.red) * perc) ,0) )
                    pixel.green = UInt8( max( min(255, Double(pixel.green) + Double(pixel.green) * perc) ,0) )
                    pixel.blue = UInt8( max( min(255, Double(pixel.blue) + Double(pixel.blue) * perc) ,0) )
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    // here the perc should be an integer [-255, 255] declaring decrease/increase in contrast
    public func changeContrast(perc: Double) {
        
        let factor = (259 * (perc + 255)) / (255 * (259 - perc))
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    let r = factor * Double(Int(pixel.red) - 128) + 128
                    pixel.red = UInt8(max(min(255, r), 0))
                    
                    let g = factor * Double(Int(pixel.green) - 128) + 128
                    pixel.green = UInt8(max(min(255, g), 0))
                    
                    let b = factor * Double(Int(pixel.blue) - 128) + 128
                    pixel.blue = UInt8(max(min(255, b), 0))
                    
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    // default filters
    public func Brightness50() {self.changeBrightness(0.5)}
    public func Brightnessminus50() {self.changeBrightness(-0.5)}
    public func Contrastx2() {self.changeContrast(2)}
    public func Contrastx4() {self.changeContrast(4)}
    
    // filter to change to grayscale
    public func grayScale() {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    let gray = UInt8( max(0, min(255, (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue))/3)))
                
                    pixel.red = gray
                    pixel.green = gray
                    pixel.blue = gray
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    // filter to change to black & white
    public func blackWhite() {
        let avgGray = self.avgColor.reduce(0, combine: +)/3
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    let gray = Int( max(0, min(255, (Int(pixel.red) + Int(pixel.green) + Int(pixel.blue))/3)))
                    // played with both choices, 
                    // using the globaly average gray which is 128...

                    //if gray < 128 {
                    
                    // ...and the image's average gray
                    if gray < avgGray {
                        // should be black
                        pixel.red = 0
                        pixel.green = 0
                        pixel.blue = 0
                    }
                    else {
                        // should be white
                        pixel.red = 255
                        pixel.green = 255
                        pixel.blue = 255
                    }
                    
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    // filter to change to sepia
    public func sepia() {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {

                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    pixel.red = UInt8( max(0, min(255, (Double(pixel.red) * 0.393 + Double(pixel.green) * 0.769 + Double(pixel.blue) * 0.189) )))
                    pixel.green = UInt8( max(0, min(255, (Double(pixel.red) * 0.349 + Double(pixel.green) * 0.686 + Double(pixel.blue) * 0.168) )))
                    pixel.blue = UInt8( max(0, min(255, (Double(pixel.red) * 0.272 + Double(pixel.green) * 0.534 + Double(pixel.blue) * 0.131) )))
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    // filter to change red channel per perc percent (+/- supported)
    public func changeChannelRed(perc: Double) {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    pixel.red = UInt8( max( min(255, Double(pixel.red) + Double(pixel.red) * perc) ,0) )
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }

    // filter to change green channel per perc percent (+/- supported)
    public func changeChannelGreen(perc: Double) {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    pixel.green = UInt8( max( min(255, Double(pixel.green) + Double(pixel.green) * perc) ,0) )
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }

    // filter to change blue channel per perc percent (+/- supported)
    public func changeChannelBlue(perc: Double) {
        let operationQueue = NSOperationQueue()
        for y in 0..<self.rgbaIm.height {
            operationQueue.addOperationWithBlock {
                for x in 0..<self.rgbaIm.width {
                    let index = y * self.rgbaIm.width + x
                    var pixel = self.rgbaIm.pixels[index]
                    
                    pixel.blue = UInt8( max( min(255, Double(pixel.blue) + Double(pixel.blue) * perc) ,0) )
                    self.rgbaIm.pixels[index] = pixel
                }
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }

    // method to apply various filters defined in filters array with an effect of the according percs %
    // if the filter has no parameter, the same i's percs should be any numeric value
    public func filterWrapper(filters: [String], percs: [Double]){
        guard filters.count == percs.count else {
            print("Wrong parameters. Count of filters must be equal to count of percentages")
            return
        }
        for i in 0..<filters.count {
            print("Filter is \(filters[i]) and percentage of change is \(percs[i])")
            switch filters[i] {
            
            case "changeBrightness":
                self.changeBrightness(percs[i])
            
            case "changeContrast":
                self.changeContrast(percs[i])
            
            case "grayScale":
                self.grayScale()
            
            case "sepia":
                self.sepia()
            
            case "blackWhite":
                self.blackWhite()
            
            case "changeChannelRed":
                self.changeChannelRed(percs[i])
            
            case "changeChannelGreen":
                self.changeChannelGreen(percs[i])
            
            case "changeChannelBlue":
                self.changeChannelBlue(percs[i])
            
            default:
                print("Filter not recognised")
            }
        }
        
    }
    
    // export the RGBA instance from the image processor
    public func getrgbaIm() -> RGBAImage {
        return self.rgbaIm
    }
}