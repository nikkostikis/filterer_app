
import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    ////////// - PROPERTIES - //////////
    
    //System variables
    var filteredImage: UIImage?
    var originalImage: UIImage?
    var rgbaOriginalImage: RGBAImage?
    var rgbaFilteredImage: RGBAImage?
    var didSelectFilter: Bool?
    var filterSelected: String?
    var ip: NKImageProc?
    let icons: [UIImage] = [
        UIImage(named: "original")!,
        UIImage(named: "br")!,
        UIImage(named: "ctr")!,
        UIImage(named: "gs")!,
        UIImage(named: "sp")!,
        UIImage(named: "bw")!,
        UIImage(named: "red")!,
        UIImage(named: "green")!,
        UIImage(named: "blue")!,
    ]
    let iconNames = [
        "original",
        "changeBrightness",
        "changeContrast",
        "grayScale",
        "sepia",
        "blackWhite",
        "changeChannelRed",
        "changeChannelGreen",
        "changeChannelBlue",
    ]
    let editAvailability = [
        false,
        true,
        true,
        false,
        false,
        false,
        true,
        true,
        true,
    ]
    let slideMultiplier = [
        1.0,
        1.0,
        255.0,
        1.0,
        1.0,
        1.0,
        1.0,
        1.0,
        1.0,
    ]
    let waitQueue: dispatch_queue_t = dispatch_queue_create("waiter", nil)

    //imageViews Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var filteredImageView: UIImageView!
    
    //menus Outlets
    @IBOutlet var secondaryMenu: UIView!
    @IBOutlet weak var bottomMenu: UIView!
    @IBOutlet var sliderMenu: UIView!
    
    //UIButtons Outlets - Some are in the old Secondary menu
    //so they are useless
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var compareButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var brButton: UIButton!
    @IBOutlet weak var ctrButton: UIButton!
    @IBOutlet weak var bwButton: UIButton!
    @IBOutlet weak var spButton: UIButton!
    @IBOutlet weak var gsButton: UIButton!
    
    //Controls Outlets
    @IBOutlet weak var editSlider: UISlider!

    //Labels Outlets
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!

    //Collection Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    
    
    
    ////////// - METHODS - //////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        initializeUI()
    }
    func initializeUI(){
        //secondaryMenu.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        //secondaryMenu.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        sliderMenu.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        sliderMenu.translatesAutoresizingMaskIntoConstraints = false
        
        initialLabel.alpha = 1
        infoLabel.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.6)
        infoLabel.alpha = 0
        disableButtons([filterButton, editButton, shareButton, compareButton])
        filteredImageView.alpha = 0
    }
    //action for the long press on screen
    @IBAction func onLongPress(recognizer: UILongPressGestureRecognizer) {
        guard didSelectFilter! == true else {return}
        if recognizer.state == UIGestureRecognizerState.Began {
            showOriginal()
            print("I am being pressed")
        }
        else {
            showFiltered()
            print("I was released")
        }
    }
 
    
    
    
    
    ////////// - button actions - //////////
    
    
    //action for the Filter button
    @IBAction func onFilter(sender: UIButton) {
        if (sender.selected) {
            hideSecondaryMenu(sender)
            sender.selected = false
        } else {
            showSecondaryMenu(sender)
            hideSecondaryMenu(editButton)
            editButton.selected = false
            sender.selected = true
        }
    }

    //action for Edit button
    @IBAction func onEdit(sender: UIButton) {
        if (sender.selected) {
            hideSecondaryMenu(sender)
            sender.selected = false
        } else {
            showSecondaryMenu(sender)
            hideSecondaryMenu(filterButton)
            filterButton.selected = false
            sender.selected = true
        }
    }
    //action for New Photo button - prepare and show the action sheet
    @IBAction func onNewPhoto(sender: UIButton) {
        hideAllMenus()
        let actionSheet = UIAlertController(title: "New Photo", message: nil, preferredStyle: .ActionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { action in
            self.showCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Album", style: .Default, handler: { action in
            self.showAlbum()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        //the alert controller needs to be only presented not dismissed and that is
        //an exception to the general rule
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    //action for the Compare button
    @IBAction func onCompare(sender: UIButton) {
        if (sender.selected) {
            showFiltered()
            sender.selected = false
        } else {
            showOriginal()
            sender.selected = true
        }
    }
    //action for the Share button
    @IBAction func onShare(sender: UIButton) {
        var shareImage: UIImage?
        if filteredImageView.image != nil {
            shareImage = filteredImageView.image!
        }
        else {
            shareImage = imageView.image!
        }
        let activityController = UIActivityViewController(activityItems: ["Check out my filtered photo!", shareImage!], applicationActivities: nil)
        presentViewController(activityController, animated: true, completion: nil)
    }

    
    
    
    
    ////////// - filtering actions - //////////
    
    
    //apply filter when a cell from the collection is selected
    func applyFilterForIndex(indexPathRow: Int) {
        guard iconNames[indexPathRow] != "original" else {
            removeFilters()
            return
        }
        filterSelected = iconNames[indexPathRow]
        didSelectFilter = true
        if !editAvailability[indexPathRow] {
            applyFilter(filterSelected!, percs: 0)
            disableButtons([editButton])
        }
        else {
            applyFilter(filterSelected!,percs: Double(editSlider.value) * slideMultiplier[indexPathRow])
            enableButtons([editButton])
        }
    }
    
    func applyFilter(filterName: String, percs: Double) {
        rgbaOriginalImage = RGBAImage(image: originalImage!)
        //define filterer class
        ip = NKImageProc(image: rgbaOriginalImage!)
        
        let activityView = showActivityView("Applying Filter...")
        dispatch_async(waitQueue) {
                //apply filter
                self.ip?.filterWrapper([filterName], percs: [percs])
            dispatch_async(dispatch_get_main_queue()) {
                //extract filtered image
                self.rgbaFilteredImage = self.ip?.getrgbaIm()
                //show filtered image
                self.filteredImageView.image = self.rgbaFilteredImage?.toUIImage()
                self.imageView.alpha = 1
                self.filteredImageView.alpha = 0
                //remove activity indicator
                activityView.removeFromSuperview()
                self.showFiltered()
            }
        }
        deselectButtons([compareButton,])
        enableButtons([compareButton,])
    }
    
    func removeFilters() {
        showOriginal()
        filterSelected = nil
        didSelectFilter = false
        infoLabel.alpha = 1
        disableButtons([compareButton, editButton])
        deselectButtons([compareButton])
        editSlider.value = 0.1
    }
    
    
    
    
    
    ////////// - slider actions - //////////
    
    //action for the Edit slider - updates the currently applied filter
    @IBAction func sliderValueDidChange(sender: UISlider) {
        applyFilter(filterSelected!, percs: Double(editSlider.value))
    }
    
    
    
    
    
    ////////// - helper methods - //////////
    
    
    func showFiltered() {
        UIView.animateWithDuration(0.7) {
            self.imageView.alpha = 0
            self.filteredImageView.alpha = 1
            self.infoLabel.alpha = 0
        }
    }
    func showOriginal() {
        UIView.animateWithDuration(0.7, animations: {
            self.infoLabel.alpha = 1
            self.imageView.alpha = 1
            self.filteredImageView.alpha = 0
            }) { completed in
                if (completed) {
                    self.imageView.alpha = 1
                    self.filteredImageView.alpha = 0
                }
        }
    }
    
    //set constraints for the secondary menus dynamically
    func showSecondaryMenu(sender: UIButton) {
        var menu: UIView
        if sender == editButton {
            menu = sliderMenu
        }
        else {
            menu = collectionView
        }
        view.addSubview(menu)
        let bottomConstraint = menu.bottomAnchor.constraintEqualToAnchor(bottomMenu.topAnchor)
        let rightConstraint = menu.rightAnchor.constraintEqualToAnchor(view.rightAnchor)
        let leftConstraint = menu.leftAnchor.constraintEqualToAnchor(view.leftAnchor)
        let heightConstraint = menu.heightAnchor.constraintEqualToConstant(70)
        
        NSLayoutConstraint.activateConstraints([bottomConstraint, rightConstraint, leftConstraint, heightConstraint])
        
        view.layoutIfNeeded()
        
        menu.alpha = 0
        UIView.animateWithDuration(0.4) {
            menu.alpha = 1
        }
    }
    func hideSecondaryMenu(sender: UIButton) {
        var menu: UIView
        if sender == editButton {
            menu = sliderMenu
        }
        else {
            menu = collectionView
        }
        UIView.animateWithDuration(0.4, animations: {
            menu.alpha = 0
            }) { completed in
                if (completed) {
                    menu.removeFromSuperview()
                }
        }
    }
    
    func hideAllMenus() {
        secondaryMenu.removeFromSuperview()
        sliderMenu.removeFromSuperview()
        deselectButtons([editButton, filterButton])
    }
    
    func disableButtons(buttons: [UIButton]) {
        for i in 0..<buttons.count {
            buttons[i].enabled = false
        }
    }
    func enableButtons(buttons: [UIButton]) {
        for i in 0..<buttons.count {
            buttons[i].enabled = true
        }
    }
    func deselectButtons(buttons: [UIButton]) {
        for i in 0..<buttons.count {
            buttons[i].selected = false
        }
    }
    
    //pick photo from the camera
    func showCamera() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .Camera
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    //pick photo from the album
    func showAlbum() {
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .PhotoLibrary
        presentViewController(cameraPicker, animated: true, completion: nil)
    }
    //call this func to update all image handlers when a new image is
    //picked either from the camera or from the album
    func newImageSelected(image: UIImage) {
        imageView.image = image
        originalImage = imageView.image
        enableButtons([filterButton, shareButton])
        initialLabel.alpha = 0
        removeFilters()
    }
    //Shows an activity running indicator, so that
    //the user knows his image is being processed
    //Shout out to Kumar Guarav who posted this in the forum
    func showActivityView(msg: String)->UIView{
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.startAnimating()
        let strLabel: UILabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.whiteColor()
        let messageFrame = UIView(frame: CGRect(origin: view.frame.origin, size: CGSize(width: 220, height: 50)))
        messageFrame.layer.cornerRadius = 10
        messageFrame.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
        messageFrame.addSubview(activityIndicator)
        messageFrame.addSubview(strLabel)
        messageFrame.layer.position = view.layer.position
        view.addSubview(messageFrame)
        return messageFrame    
    }
    
    
    
    
    
    ////////// - protocol overrides - //////////
    

    //UIImagePickerControllerDelegate protocol method
    //new photo selected (doesn't matter through which picker)
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dismissViewControllerAnimated(true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //call this func to update all image handlers
            newImageSelected(image)
        }
    }
    //UIImagePickerControllerDelegate protocol method
    //new photo wasn't selected but cancelled (doesn't matter through which picker)
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //it's always good to dismiss the view controller you have shown
        dismissViewControllerAnimated(true, completion: nil)
    }
    //UICollectionViewDataSource protocol method
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return icons.count
    }
    //UICollectionViewDataSource protocol method
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("IconCell", forIndexPath: indexPath) as! CollectionViewCell
        cell.imageView.image = icons[indexPath.row]
        return cell
    }
    //UICollectionViewDelegate protocol method
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print(iconNames[indexPath.row])
        //cal wrapper method to apply filters through icons using the 
        //selected cell index
        applyFilterForIndex(indexPath.row)
    }
    

    
    
    
    ////////// - auto generated - //////////
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}

