import UIKit

public struct Pixel {
    public var value: UInt32
    public var red: UInt8 {
        get {
            return UInt8(value & 0xFF)
        }
        set {
            value = UInt32(newValue) | (value & 0xFFFFFF00)               }
    }
    
    public var green: UInt8 {
        get {
            return UInt8((value >> 8) & 0xFF)
        }
        set {
            value = (UInt32(newValue) << 8) | (value & 0xFFFF00FF)        }
    }
    
    public var blue: UInt8 {
        get {
            return UInt8((value >> 16) & 0xFF)
        }
        set {
            value = (UInt32(newValue) << 16) | (value & 0xFF00FFFF)
        }
    }
    
    public var alpha: UInt8 {
        get {
            return UInt8((value >> 24) & 0xFF)
        }
        set {
            value = (UInt32(newValue) << 24) | (value & 0x00FFFFFF)
        }
    }
}

//public struct RGBAImage {
//The RGBAImage is no longer a struct but a class
//to avoid memory leaks when applying many filters ande changing
//images.
//Shout out to David Lin for suggesting this change in the forum.
public class RGBAImage {
    private var orientation: UIImageOrientation
    private var scale: CGFloat
    
    public var pixels: UnsafeMutableBufferPointer<Pixel>
    
    public var width: Int
    public var height: Int
    
    public init?(image: UIImage) {
        //guard let cgImage = image.CGImage else { return nil }
        let cgImage = image.CGImage
        // Redraw image for correct pixel format
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        var bitmapInfo: UInt32 = CGBitmapInfo.ByteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.PremultipliedLast.rawValue & CGBitmapInfo.AlphaInfoMask.rawValue
        
        var imageSize:CGSize
        if (image.imageOrientation != .Up && image.imageOrientation != .Down) {
            imageSize = CGSize(width: image.size.height, height: image.size.width)
        }else{
            imageSize=image.size
        }
        
        width = Int(imageSize.width)
        height = Int(imageSize.height)
        orientation = image.imageOrientation
        scale = image.scale
        let bytesPerRow = width * 4
        
        let imageData = UnsafeMutablePointer<Pixel>.alloc(width * height)
        
        /*guard let imageContext = CGBitmapContextCreate(imageData, width, height, 8, bytesPerRow, colorSpace, bitmapInfo) else { return nil }*/
        let imageContext = CGBitmapContextCreate(imageData, width, height, 8, bytesPerRow, colorSpace, bitmapInfo)
        CGContextDrawImage(imageContext, CGRect(origin: CGPointZero, size: imageSize), cgImage)
        pixels = UnsafeMutableBufferPointer<Pixel>(start: imageData, count: width * height)
        
        
    }
    
    deinit {
        print("Deinit is called")
    }
    
    public func toUIImage() -> UIImage? {
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        var bitmapInfo: UInt32 = CGBitmapInfo.ByteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.PremultipliedLast.rawValue & CGBitmapInfo.AlphaInfoMask.rawValue
        
        let bytesPerRow = width * 4
        let imageContext = CGBitmapContextCreateWithData(pixels.baseAddress, width, height, 8, bytesPerRow, colorSpace, bitmapInfo, nil, nil)
        
        guard let cgImage = CGBitmapContextCreateImage(imageContext) else {return nil}
        let image = UIImage(CGImage: cgImage, scale: scale, orientation: orientation)
        
        return image
    }
}